package com.asimio.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiDockerDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiDockerDemoApplication.class, args);
	}
}
