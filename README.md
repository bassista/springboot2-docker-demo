# README #

Demo building a Docker image for a Spring Boot 2 application using Jib accompanying source code for blog post at https://tech.asimio.net/2018/08/10/Simplifying-packaging-Spring-Boot-2-applications-into-Docker-images-using-Google-Jib.html

### Requirements ###

* Java 8
* Maven 3.2.x

### Building the image ###

```
mvn clean versions:set -DnewVersion=1.0.1
mvn package
# Update settings.xml with auth credentials to be able to push image to registry.hub.docker.com
mvn jib:build
```

### Running a container ###

```
docker run asimio/springboot2-docker-demo:1.0.1
```

### Who do I talk to? ###

* orlando.otero at asimio dot net
* https://www.linkedin.com/in/ootero
